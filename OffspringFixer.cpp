#include "OffspringFixer.h"

/*===========================================================*/
/*                        CONSTRUCTOR                        */
/*===========================================================*/

/**
 * Constructor storing a reference of existing TInvidu (to allow direct modification) and a position vector to have constant-time access to the position of any given element.
 */
OffspringFixer::OffspringFixer(TIndividu &offspring): offspring(offspring) {
	scanPositionsFromOffspring();
}

/**
 * Populates position vector by scanning offspring's DNA sequence.
 * Example: if element "5" is at position 22 in offspring's DNA sequence, then positions[5] = 22.
 */
void OffspringFixer::scanPositionsFromOffspring() {

	const unsigned int sequenceSize = this->offspring.Seq.size();
	this->positions = vector<int>(sequenceSize);
	
	// For each element in offspring's DNA sequence, save its position in the position vector
	for(unsigned int i=0; i < sequenceSize; i++) {
		const int currentValue = this->offspring.Seq[i];
		this->positions[currentValue] = i;
	}
}

/*===========================================================*/
/*                         DNA FIXING                        */
/*===========================================================*/

void OffspringFixer::validateConstraints(Constraints c) {
	const vector<int> successorsList = c.getSuccessors();
	int predecessorValue = c.getPredecessor();
	int predecessorPosition = this->getPositionOf(predecessorValue);
	
	// For each successor of given predecessor, check their respective positions
	for(unsigned int i=0; i < c.getSuccessorsCount(); i++) {
		int currentSuccessorValue = successorsList[i];
		int currentSuccessorPosition = this->getPositionOf(currentSuccessorValue);

		// If successor appears before predecessor, swap positions of the 2 given elements
		if(currentSuccessorPosition < predecessorPosition) {
			//cout << endl << "	[DEBUG] Predecessor (" << predecessorValue << ") found at position " << predecessorPosition << " while successor (" << currentSuccessorValue << ") found at position " << currentSuccessorPosition << endl; 
			swapElement(predecessorValue, predecessorPosition, currentSuccessorValue, currentSuccessorPosition);
			//cout << "	[DEBUG] POST-SWAP Predecessor (" << predecessorValue << ") found at position " << this->getPositionOf(predecessorValue) << " while successor (" << currentSuccessorValue << ") found at position " << this->getPositionOf(currentSuccessorValue) << endl; 
		}
	}
}

/*===========================================================*/
/*                           UTILS                           */
/*===========================================================*/

/**
 * Returns the position of given element.
 */
int OffspringFixer::getPositionOf(const int element) const {
	return this->positions[element];
}

/**
 * Swap positions of the 2 given elements in both offspring's DNA sequence and position vector 
 */
void OffspringFixer::swapElement(const int predecessorValue, const int predecessorPosition, const int successorValue, const int successorPosition) {
	
	//Update predecessor position
	this->offspring.Seq[predecessorPosition] = successorValue;
	this->positions[predecessorValue] = successorPosition;

	//Update successor position
	this->offspring.Seq[successorPosition] = predecessorValue;
	this->positions[successorValue] = predecessorPosition;
}
#include "ValidationRules.h"

/*===========================================================*/
/*                        CONSTRUCTOR                        */
/*===========================================================*/

/**
 * Singleton-style accessor, allowing private constructor to be called EXACTLY once.
 */
ValidationRules& ValidationRules::getValidationRules(const TProblem &tProblem) {
    static ValidationRules instance(tProblem);
    return instance;
}

/**
 * Private constructor parsing successors array to only keep relevant constraints.
 */
ValidationRules::ValidationRules(const TProblem &instance) {

	// For each element in instance's successor array, check for the existence of successors
	//	 - [At least one successor]: Create new constraint with given predecessor and iterate over its sucessors
	//	 - [No successor]: Do nothing
	for(unsigned int i=0; i < instance.Succ.size(); i++) {
		if(instance.Succ[i].size() > 0) {
			Constraints c(i);
			for(unsigned int s=0; s < instance.Succ[i].size(); s++) {
				c.addSucessor(instance.Succ[i][s]);
			}
			addConstraints(c);
		}
	}
	sortConstraints();
}

/*===========================================================*/
/*                        CONSTRAINTS                        */
/*===========================================================*/

void ValidationRules::addConstraints(const Constraints &c) {
	this->rules.push_back(c);
}

/**
 * Sort constraints based on their number of successors (see @Constraints)
 */
void ValidationRules::sortConstraints() {
	std::sort(this->rules.begin(), this->rules.end());
}

/*===========================================================*/
/*                         DNA FIXING                        */
/*===========================================================*/

/**
 * Fix offspring's DNA sequence by sending the reference of the offspring into an OffspringFixer.
 * This process iterates over each constraint to verify if they haven't been enforced.
 */
void ValidationRules::fixOffspring(TIndividu &offspring) {
	OffspringFixer offspringFixer = OffspringFixer(offspring);
	for(unsigned int i=0; i < this->rules.size(); i++) {
		offspringFixer.validateConstraints(rules[i]);
	}
}

void ValidationRules::fixOffspringBackwards(TIndividu &offspring) {
	OffspringFixer offspringFixer = OffspringFixer(offspring);
	const int lastIndex = this->rules.size() - 1;
	for(int i=lastIndex; i >= 0; i--) {
		offspringFixer.validateConstraints(rules[i]);
	}
}

/*===========================================================*/
/*                           UTILS                           */
/*===========================================================*/

/**
 * Utility method allowing rules display (each predecessor and all its successors)
 */
void ValidationRules::printRules() {
	for(unsigned int i=0; i < this->rules.size(); i++) {
		const Constraints &c = this->rules[i];
		cout << "	[RULE #" << i << "] ";
		printPredecessor(c);
		printSuccessors(c);
	}
}

const void ValidationRules::printPredecessor(const Constraints &c) const {
	cout << "Element " << c.getPredecessor() << " has " << c.getSuccessorsCount() << " successor(s): ";
}

const void ValidationRules::printSuccessors(const Constraints &c) const {
	const vector<int> suc = c.getSuccessors();
		for(unsigned int j=0; j < c.getSuccessorsCount(); j++) {
			cout << suc[j] << " ";
		}
		cout << endl;
}
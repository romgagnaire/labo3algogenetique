#pragma once

#include <algorithm>
#include "Entete.h"
#include "Constraints.h"
#include "OffspringFixer.h"

using namespace std;

/**
 * Class used to retrieve constraints from the problem instance.
 * Uses a singleton implementation to make sure rules are only created once.
 */
class ValidationRules
{
	public:
		//Singleton-style constructor
		static ValidationRules& getValidationRules(const TProblem &tProblem);
		
		//Public methods
		void fixOffspring(TIndividu &offspring);
		void fixOffspringBackwards(TIndividu &offspring);
		void printRules();

	private:
		//Attribute members
		vector<Constraints> rules;

		//Private constructor
		ValidationRules(const TProblem &tProblem);
		
		//Private methods
		void addConstraints(const Constraints &c);
		void sortConstraints();
		bool validateConstraints(OffspringFixer &offspringFixer);
		const void printPredecessor(const Constraints &c) const;
		const void printSuccessors(const Constraints &c) const;
};
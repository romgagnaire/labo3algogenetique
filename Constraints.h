#pragma once

#include <vector>

using namespace std;

/**
 * Class used to define constraints concerning one specific predecessor and all its sucessors.
 */
class Constraints
{
	public:
		//Constructor
		Constraints(const int predecessor);
		const void Constraints::addSucessor(const int successor);

		//Public methods
		const int& getPredecessor() const;
		const vector<int> &getSuccessors() const;
		const size_t Constraints::getSuccessorsCount() const;
		friend bool operator<(const Constraints& l, const Constraints& r);

	private:
		//Attribute members
		int predecessor;
		vector<int> successors;
};


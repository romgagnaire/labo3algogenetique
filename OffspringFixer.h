#pragma once

#include "Entete.h"
#include "Constraints.h"

/**
 * Class used to fix an offspring's DNA by iterative validation of constraints.
 */
class OffspringFixer
{
	public:
		OffspringFixer(TIndividu &offspring);
		void validateConstraints(Constraints c);

	private:
		//Attribute members
		TIndividu &offspring;
		vector<int> positions;

		//Private methods
		void scanPositionsFromOffspring();
		int getPositionOf(const int element) const;
		void swapElement(const int predecessorValue, const int predecessorPosition, const int successorValue, const int successorPosition);
};
